﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica5 {
    public partial class Form1 : Form {
        Hash hash;
        string variableWithHash;

        public Form1() {
            hash = new Hash();
            variableWithHash = "";
            InitializeComponent();
        }

        private void ButtonAdd_Click(object sender, EventArgs e) {
            string variable = textBoxVariable.Text;

            // si el textbox no está vacío
            if (!string.IsNullOrWhiteSpace(variable)) {
                if (dataGridView.Rows.Count - 1 != 250) {
                    variableWithHash = hash.GenerateHash(variable);

                    int collisionIndex = hash.VerifyCollision(dataGridView, variableWithHash);

                    // si hay colisión
                    if (collisionIndex != -1) {
                        // obtenemos el hash de la variable que hace colisión
                        string oldHash = dataGridView.Rows[collisionIndex].Cells[0].Value.ToString();

                        // pasamos por otro hash
                        variableWithHash = hash.GenerateHash2(dataGridView, oldHash);

                        // insertamos la colisión
                        dataGridView.Rows[collisionIndex].Cells[2].Value += variableWithHash + " ";
                    }

                    DataGridViewRow row = (DataGridViewRow)dataGridView.Rows[0].Clone();
                    row.Cells[0].Value = variableWithHash; // id de la variable
                    row.Cells[1].Value = variable; // valor de la variable
                    row.Cells[2].Value = ""; // colisión
                    dataGridView.Rows.Add(row);


                    // limpiamos textbox
                    textBoxVariable.Text = "";
                } else {
                    MessageBox.Show(
                    "Todo lleno :c",
                    "Advertencia",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );
                }
            } else {
                MessageBox.Show(
                    "El campo variable no puede quedar vacío",
                    "Advertencia",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );
            }
        }
    }
}
