﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Practica5 {
    class Hash {
        int contador;

        public Hash() {
            contador = 0;
        }

        // función que genera un hash
        public string GenerateHash(string variable) {
            variable = ConvertCharsToNumbers(variable);
            variable = GetSumOfCharacters(variable);

            return variable;
        }
        
        // función para generar un segundo hash
        public string GenerateHash2(DataGridView dataGridView, string oldHash) {
            long dateTime = int.Parse(DateTime.Now.ToString("ddMMhhmmss")) / 20000000;

            string newHash = (int.Parse(oldHash) + dateTime).ToString();

            // verificamos si hay colisión en este segundo algorítmo
            if (VerifyCollision(dataGridView, newHash) != -1) {
                newHash = GenerateHash2(dataGridView, newHash);

                if (int.Parse(newHash) > 250) {
                    newHash = (250 - contador).ToString();
                    contador += 1;
                }
            }

            return newHash;
        }

        // función que verifica si hay una colisión
        public int VerifyCollision(DataGridView dataGridView, string variable) {
            foreach (DataGridViewRow thisRow in dataGridView.Rows) {
                // prevenimos error por si se sale de la columna
                if (thisRow.Cells[0].Value != null) {
                    if (thisRow.Cells[0].Value.ToString() == variable) {
                        // hubo colisión
                        return thisRow.Index;
                    }
                }
            }

            // todo salió bien :D
            return -1;
        }

        // función que suma todos los caracteres de una cadena
        private string GetSumOfCharacters(string variableHash) {
            //variableHash = Regex.Replace(variableHash, @"\s+", "");
            int sum = variableHash.Select(c => int.Parse(c.ToString())).Sum();
            string sumString = sum + "";

            return sumString;
        }

        // función que convierte caracteres a números
        private string ConvertCharsToNumbers(string variable) {
            variable = variable.Replace(" ", "");
            variable = variable.Replace("0", "53");
            variable = variable.Replace("1", "54");
            variable = variable.Replace("2", "55");
            variable = variable.Replace("3", "56");
            variable = variable.Replace("4", "57");
            variable = variable.Replace("5", "58");
            variable = variable.Replace("6", "59");
            variable = variable.Replace("7", "60");
            variable = variable.Replace("8", "61");
            variable = variable.Replace("9", "62");
            variable = variable.Replace("a", "1");
            variable = variable.Replace("b", "2");
            variable = variable.Replace("c", "3");
            variable = variable.Replace("d", "4");
            variable = variable.Replace("e", "5");
            variable = variable.Replace("f", "6");
            variable = variable.Replace("g", "7");
            variable = variable.Replace("h", "8");
            variable = variable.Replace("i", "9");
            variable = variable.Replace("j", "10");
            variable = variable.Replace("k", "11");
            variable = variable.Replace("l", "12");
            variable = variable.Replace("m", "13");
            variable = variable.Replace("n", "14");
            variable = variable.Replace("o", "15");
            variable = variable.Replace("p", "16");
            variable = variable.Replace("q", "17");
            variable = variable.Replace("r", "18");
            variable = variable.Replace("s", "19");
            variable = variable.Replace("t", "20");
            variable = variable.Replace("u", "21");
            variable = variable.Replace("v", "22");
            variable = variable.Replace("w", "23");
            variable = variable.Replace("x", "24");
            variable = variable.Replace("y", "25");
            variable = variable.Replace("z", "26");
            variable = variable.Replace("A", "27");
            variable = variable.Replace("B", "28");
            variable = variable.Replace("C", "29");
            variable = variable.Replace("D", "30");
            variable = variable.Replace("E", "31");
            variable = variable.Replace("F", "32");
            variable = variable.Replace("G", "33");
            variable = variable.Replace("H", "34");
            variable = variable.Replace("I", "35");
            variable = variable.Replace("J", "36");
            variable = variable.Replace("K", "37");
            variable = variable.Replace("L", "38");
            variable = variable.Replace("M", "39");
            variable = variable.Replace("N", "40");
            variable = variable.Replace("O", "41");
            variable = variable.Replace("P", "42");
            variable = variable.Replace("Q", "43");
            variable = variable.Replace("R", "44");
            variable = variable.Replace("S", "45");
            variable = variable.Replace("T", "46");
            variable = variable.Replace("U", "47");
            variable = variable.Replace("V", "48");
            variable = variable.Replace("W", "49");
            variable = variable.Replace("X", "50");
            variable = variable.Replace("Y", "51");
            variable = variable.Replace("Z", "52");

            return variable;
        }
    }
}
